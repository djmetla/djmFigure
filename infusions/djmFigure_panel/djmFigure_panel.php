<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: djmFigure_panel.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied"); }

function djmFigure($buffer) {
	preg_match_all('#<a.*?href=\''.BASEDIR.'profile\.php\?lookup=([0-9]+)\'.*?>#', $buffer, $matches);
	
	$match= array_unique($matches[1]);
	if (!empty($match)) {
		$users = "";
		foreach ($match as $v) { $users .= ($users ? ',' : '').$v; }	
		unset($match);
		
		$query = dbquery("SELECT t1.*, t2.user_name, t3.* 
						  FROM ".DB_PREFIX."figure as t1 
						  LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.figure_user 
						  LEFT JOIN ".DB_PREFIX."figure_category as t3 ON t3.category_id=t1.figure_category 
						  WHERE figure_user IN (".$users.")");

		while ($data = dbarray($query)) {
			$buffer = preg_replace(
				'#<a(.*?)href=\''.BASEDIR.'profile\.php\?lookup='.$data['figure_user'].'\'(.*?)>(.*?)</a>#s', 
				'<a\\1href=\''.BASEDIR.'profile.php?lookup='.$data['figure_user'].'\'\\2 style="color: #'.$data['category_color'].'; font-weight: '.($data['category_bold'] == 1 ? 'bold':'normal').';">\\3</a> '.($data['category_image'] ? '<img alt=\''.$data['category_name'].'\' title=\''.$data['category_name'].'\' src="'.INFUSIONS.'djmFigure_panel/images/'.$data['category_image'].'" />':'').'', $buffer);
		}
	} 

	return $buffer;
}

add_handler('djmFigure');
?>