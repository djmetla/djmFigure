<?php

$t[1] = "U��vate�sk� hodnosti zobrazen� pan��ikmi za nickom";
$t[2] = "V poriadku. Do zlo�ky sa d� zapisova�.";
$t[3] = "Ne�spe�ne. Nastavte chmod zlo�ky na 0777!";
$t[4] = "V poriadku, nain�talovan�!";
$t[5] = "Aktivova� panel";
$t[6] = "In�tal�cia";
$t[7] = "Opr�vnenia zlo�ky <b>djmFigure_panel/images</b> (chmod)";
$t[8] = "Povoli� panel <b>djmFigure_panel</b> (<span style='color: red;'>Vy�aduje pre spr�vnu funk�nos� modifik�cie!!!</span>)";
$t[9] = "Kateg�ria sa ned� zmaza�, preto�e obsahuje u��vate�ov!";
$t[10] = "Upravi� u��vate�a";
$t[11] = "U��vate�";
$t[12] = "Kateg�ria";
$t[13] = "Upravi�";
$t[14] = "Upravi� kateg�riu";
$t[15] = "N�zov kateg�rie";
$t[16] = "U��vatelia";
$t[17] = "Mo�nosti";
$t[18] = "Zmaza�";
$t[19] = "Vytvori� u��vate�a";
$t[20] = "N�zov kateg�rie";
$t[21] = "Obr�zok kateg�rie";
$t[22] = "Po�et u��vate�ov";
$t[23] = "Vytvori� kateg�riu";
$t[24] = "Kateg�rie";
$t[25] = "Nahran� obr�zky";
$t[26] = "Obr�zok";
$t[27] = "N�zov obr�zku";
$t[28] = "Nahra�";
$t[29] = "Zlo�ka <b>djmFigure_panel/images</b> nieje nastaven� pre zapisovanie. Zme�te opr�vnenia zlo�ky (chmod) na 0777. V opa�nom pr�pade nebudete m�c� nahr�va� obr�zky do tejto zlo�ky.";

# Update 20.1.2013
$t[30] = "V poriadku, update aplikovan�!";
$t[31] = "Aplikova� zmeny";
$t[32] = "Aplikova� zmeny novej verzie";
$t[33] = "Aktualiz�cia bola �spe�ne aplikovan�. Odteraz pou�ivate najnov�iu verziu modifik�cie djmLeague.";
$t[34] = "Zavrie� spr�vu";
$t[35] = "Vyberte farbu";
$t[36] = "Tu�n� p�smo";
$t[37] = "Farba";
$t[38] = "Tu�n�";
$t[39] = "�no";
$t[40] = "Nie";
$t[41] = "Varovanie";
$t[42] = "Bola zisten� nov� verzia modifik�cie. Aplikujte zmeny v panely in�tal�cia (hore). Bez aplikovania aktualiz�cie nie je mo�n� pou�iva� modifik�ciu!";

# Update 03.02.2014
$t[43] = "�iadny obr�zok";
$t[44] = "Dostupn� aktualiz�cia";
$t[45] = "Aktu�lna verzia";
$t[46] = "Najnov�ia verzia";
$t[47] = "Bola zisten� aktualiz�cia";
$t[48] = "Syst�m zistil dostupnos� novej verzie.";
$t[49] = "djmFigure v najnov�ej verzii";
$t[50] = "stiahnete prostredn�ctvom tohoto odkazu";