<?php

$t[1] = "Uživatelské hodnosti zobrazené panáčky za nickem";
$t[2] = "V pořádku. Do složky se dá zapisovat.";
$t[3] = "Neúspěšně. Nastavte chmod složky na 0777!";
$t[4] = "V pořádku, nainstalovaný!";
$t[5] = "Aktivovat panel";
$t[6] = "Instalace";
$t[7] = "Oprávnění složky <b>djmFigure_panel/images</b> (chmod)";
$t[8] = "Povolit panel <b>djmFigure_panel</b> (<span style='color: red;'>Vyžaduje pro správnou funkčnost modifikace!!!</span>)";
$t[9] = "Kategorie se nedá smazat, protože obsahuje uživatele!";
$t[10] = "Upravit uživatele";
$t[11] = "Uživatel";
$t[12] = "Kategorie";
$t[13] = "Upravit";
$t[14] = "Upravit kategorii";
$t[15] = "Název kategorie";
$t[16] = "Uživatelé";
$t[17] = "Možnosti";
$t[18] = "Smazat";
$t[19] = "Vytvořit uživatele";
$t[20] = "Název kategorie";
$t[21] = "Obrázek kategorie";
$t[22] = "Počet uživatelů";
$t[23] = "Vytvořit kategorii";
$t[24] = "Kategorii";
$t[25] = "Nahrané obrázky";
$t[26] = "Obrázek";
$t[27] = "Název obrázku";
$t[28] = "Nahrát";
$t[29] = "Složka <b>djmFigure_panel/images</b> není nastavená pro zapisování. Změňte oprávnění složky (chmod) na 0777. V opačném případě nebudete moci nahrávat obrázky do této složky.";

# 20.1.2013
$t[30] = "V pořádku, update aplikovaný!";
$t[31] = "Aplikovat změny";
$t[32] = "Aplikovat změny nové verze";
$t[33] = "Aktualizace byla úspěšně aplikovaná. Odteď používáte nejnovější verzi modifikace djmLeague.";
$t[34] = "Zavřít zprávu";
$t[35] = "Vyberte barvu";
$t[36] = "Tučné písmo";
$t[37] = "Barva";
$t[38] = "Tučné";
$t[39] = "Ano";
$t[40] = "Ne";
$t[41] = "Varování";
$t[42] = "Byla zjištěna nová verze modifikace. Aplikujte změny v panelu instalace (nahoře). Bez aplikování aktualizace není možné používat modifikaci!";

# 03.03.2014
$t[43] = "Žádný obrázek";
$t[44] = "Dostupná aktualizace";
$t[45] = "Aktuální verze";
$t[46] = "Nejnovější verze";
$t[47] = "Byla zjištěna aktualizace";
$t[48] = "Systém zjistil dostupnost nové verze.";
$t[49] = "djmFigure v nejnovější verzi";
$t[50] = "stáhněte prostřednictvím tohoto odkazu";
